// Copyright 2025 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package resolve

// Value represents a variable value in the interpreter.
type Value interface {
}
