// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"encoding/csv"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

var (
	// The Git revision to benchmark at.
	revision = flag.String("revision", "HEAD", "The Git revision to benchmark at.")

	// The number of iterations to run the benchmark for.
	runs = flag.Int("runs", 1, "The number of iterations to run the benchmark for.")

	// The number of different commits to run the benchmark at.
	numCommits = flag.Int("commits", 1, "How many commits to run the benchmark at.")

	// Whether to skip commits generated by robots.
	skipRobots = flag.Bool("skip_robots", true, "Whether to skip commits generated by robots (only used when --commits > 1).")

	// Which output directory to use.
	outputDir = flag.String("output_dir", "out/benchmark", "Which output directory to use.")

	// Clean the workspace before running the benchmark.
	clean   = flag.Bool("clean", false, "Clean the output directory before running the benchmark.")
	clobber = flag.Bool("clobber", false, "Build with clobber mode enabled.")

	// gn args to use when building.
	useSiso       = flag.Bool("use_siso", true, "Use Siso instead of Ninja.")
	useRemoteExec = flag.Bool("use_remoteexec", true, "Use remote execution.")
	useReclient   = flag.Bool("use_reclient", true, "Use reclient for remote execution.")

	// Various tunables.
	racing        = flag.Bool("racing", true, "Enable racing when using reclient, and fastlocal when using Siso.")
	depsCache     = flag.Bool("deps_cache", true, "Enable the deps cache.")
	localFallback = flag.Bool("local_fallback", true, "Enable local fallback when actions fail remotely.")
	gnThreads     = flag.Int("gn_threads", 8, "How many threads gn should use. Empirically, 8 seems to be the sweet spot on most machines.")
	alwaysGnGen   = flag.Bool("always_gn_gen", false, "Always run 'gn gen', even if the build files are already up-to-date.")
	alwaysSync    = flag.Bool("always_sync", false, "Always run 'gclient sync', even if the workspace is already at the necessary commit.")

	// Paths to various important locations.
	workspaceRoot = flag.String("chromium_dir", ".", "Where to find the Chromium checkout.")
	depotTools    = flag.String("depot_tools", "", "Where to find the depot_tools.")
	resultsPath   = flag.String("results", "$HOME/results.csv", "Where to store the results.")
	results       *csv.Writer

	// The targets to build.
	targets []string
)

// parseFlags parses the command-line flags and performs some validation of all the provided
// values.
func parseFlags() (err error) {
	flag.Parse()

	// Check the flags for conflicts and errors.
	if *runs < 1 {
		return fmt.Errorf("invalid number of runs")
	}

	if *numCommits < 1 {
		return fmt.Errorf("invalid number of commits")
	}

	if *gnThreads < 1 {
		return fmt.Errorf("invalid number of gn threads")
	}

	if *clean && *clobber {
		return fmt.Errorf("cannot use both -clean and -clobber")
	}

	if *useReclient && *racing && !*localFallback {
		return fmt.Errorf("when using reclient, -racing=true and -local_fallback=false cannot be used together")
	}

	if !*useSiso && *useRemoteExec && !*useReclient {
		return fmt.Errorf("ninja cannot use remote execution without reclient")
	}

	if !*useSiso && *clobber {
		return fmt.Errorf("ninja cannot use clobber mode")
	}

	if !filepath.IsLocal(*outputDir) {
		return fmt.Errorf("output directory must be relative and underneath the workspace root")
	}

	// The targets are the remaining arguments.
	targets = flag.Args()
	if len(targets) == 0 {
		targets = []string{"all"}
	}

	// Ensure that the results path is expanded, as it may contain environment variables like $HOME.
	*resultsPath = os.ExpandEnv(*resultsPath)

	// Ensure that the depotTools are available and up-to-date.
	*depotTools, err = ensureDepotTools(*depotTools)
	if err != nil {
		return err
	}

	// Ensure that we are inside a Chromium checkout.
	*workspaceRoot, err = ensureChromiumWorkspace(*workspaceRoot)
	if err != nil {
		return err
	}

	return nil
}

// ensureDepotTools ensures that the depot_tools are available, up-to-date, and disables automatic
// updates during the benchmark.
func ensureDepotTools(dtDir string) (string, error) {
	var err error
	if dtDir == "" {
		// Find depotTools in the PATH by looking for the "gclient" command.
		dtDir, err = exec.LookPath("gclient")
		if err != nil {
			return "", fmt.Errorf("failed to lookup depot_tools in PATH: %w", err)
		}
		dtDir = filepath.Dir(dtDir)
	}

	// Update depot_tools once by executing the "update_depot_tools" script.
	// Explicitly set UPDATE_DEPOT_TOOLS=1 just for this call to force the update.
	cmd := exec.Command(filepath.Join(dtDir, "update_depot_tools"))
	cmd.Env = append(os.Environ(), "UPDATE_DEPOT_TOOLS=1")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err = cmd.Run(); err != nil {
		return "", fmt.Errorf("failed to update depot_tools: %w", err)
	}

	return dtDir, nil
}

// ensureChromiumWorkspace ensures that we are inside a Chromium workspace.
func ensureChromiumWorkspace(dir string) (string, error) {
	if err := os.Chdir(dir); err != nil {
		return "", fmt.Errorf("failed to change to directory %q: %w", dir, err)
	}

	// Check if we are inside a Chromium workspace by looking for the presence of some files.
	// TODO: Add support for benchmarking in non-git workspaces.
	for _, file := range []string{"../.gclient", "BUILD.gn", ".git"} {
		if _, err := os.Stat(file); err != nil {
			if !errors.Is(err, os.ErrNotExist) {
				return "", fmt.Errorf("failed to stat file %q: %w", file, err)
			}
			return "", fmt.Errorf("expected file %q to exist - please ensure to run this tool in the root of a Chromium workspace", file)
		}
	}

	return os.Getwd()
}
