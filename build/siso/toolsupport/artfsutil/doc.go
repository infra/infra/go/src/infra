// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package artfsutil provides access to artfs.
// See
//
//	sso://gbt/artfs - source code
//	http://shortn/_VSlSucYbAC - Getting started with the artfs prototype
package artfsutil
