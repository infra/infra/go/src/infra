// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cogutil provides access to cog system.
// See
//
//	http://shortn/_i28Te0webh - unconditional writes, local redirect mode
//	http://shortn/_rBwmJ3sRKS - buildfs prototype
package cogutil
