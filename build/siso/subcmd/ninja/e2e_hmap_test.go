// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ninja

import (
	"context"
	"fmt"
	"path"
	"testing"

	rpb "github.com/bazelbuild/remote-apis/build/bazel/remote/execution/v2"

	"go.chromium.org/infra/build/siso/build"
	"go.chromium.org/infra/build/siso/hashfs"
	"go.chromium.org/infra/build/siso/reapi/reapitest"
)

func TestBuild_Hmap(t *testing.T) {
	ctx := context.Background()
	dir := t.TempDir()

	ninja := func(t *testing.T, refake *reapitest.Fake) (build.Stats, error) {
		t.Helper()
		var ds dataSource
		defer func() {
			err := ds.Close(ctx)
			if err != nil {
				t.Error(err)
			}
		}()
		ds.client = reapitest.New(ctx, t, refake)
		ds.cache = ds.client.CacheStore()

		opt, graph, cleanup := setupBuild(ctx, t, dir, hashfs.Option{
			StateFile:  ".siso_fs_state",
			DataSource: ds,
		})
		defer cleanup()
		opt.REAPIClient = ds.client
		return runNinja(ctx, "build.ninja", graph, opt, nil, runNinjaOpts{})
	}

	setupFiles(t, dir, t.Name(), nil)
	fakere := &reapitest.Fake{
		ExecuteFunc: func(fakere *reapitest.Fake, action *rpb.Action) (*rpb.ActionResult, error) {
			tree := reapitest.InputTree{CAS: fakere.CAS, Root: action.InputRootDigest}
			var d *rpb.Digest
			for _, fname := range []string{
				"../../ios/foo.mm",
				"gen/ios/AppFramework.headers.hmap",
				"../../ios/AppFramework/Action.h",
				"../../ios/AppFramework/App.h",
			} {
				fn, err := tree.LookupFileNode(ctx, path.Join("out/siso", fname))
				if err != nil {
					t.Logf("missing %s in input", fname)
					return &rpb.ActionResult{
						ExitCode:  1,
						StderrRaw: []byte(fmt.Sprintf("%s: File not found: %v", fname, err)),
					}, nil
				}
				t.Logf("input %s is ok", fname)
				if fname == "../../ios/foo.mm" {
					d = fn.Digest
				}
			}
			dd, err := fakere.Put(ctx, []byte("obj/ios/foo.o: ../../ios/foo.mm\n"))
			if err != nil {
				msg := fmt.Sprintf("failed to write obj/ios/foo.o.d: %v", err)
				t.Log(msg)
				return &rpb.ActionResult{
					ExitCode:  1,
					StderrRaw: []byte(msg),
				}, nil
			}
			return &rpb.ActionResult{
				ExitCode: 0,
				OutputFiles: []*rpb.OutputFile{
					{
						Path:   "obj/ios/foo.o",
						Digest: d,
					},
					{
						Path:   "obj/ios/foo.o.d",
						Digest: dd,
					},
				},
			}, nil
		},
	}
	stats, err := ninja(t, fakere)
	if err != nil {
		t.Fatalf("ninja %v: want nil err", err)
	}
	if stats.Done != stats.Total || stats.Remote != 1 {
		t.Errorf("done=%d remote=%d total=%d; want done=total, remote=1: %#v", stats.Done, stats.Remote, stats.Total, stats)
	}
}
