# Copyright 2024 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


def main():
  # generate_fontconfig_caches generates fontconfig_caches/cache.
  with open("fontconfig_caches/cache", "w") as w:
    w.write('generated by generate_fontconfig_caches')
