// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package consoleserver is the implementation of the backend of the console UI.
package consoleserver
