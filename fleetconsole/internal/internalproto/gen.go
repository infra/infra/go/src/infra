// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package internalproto is the generated protos used internally.
package internalproto

//go:generate cproto -use-grpc-plugin
