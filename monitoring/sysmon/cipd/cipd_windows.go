// Copyright (c) 2016 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package cipd

var versionDirs = []string{
	"C:\\infra-python3",
	"C:\\infra-tools", // luci-auth cipd version file is here
	"C:\\infra-tools\\.versions",
}
