// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package `entities` contains go structs for all the
// entities required to be read from the datastore.
package entities
