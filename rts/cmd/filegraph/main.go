// Copyright 2020 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import "go.chromium.org/infra/rts/filegraph/cli"

func main() {
	cli.Main()
}
