// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.5
// 	protoc        v5.29.3
// source: go.chromium.org/infra/tools/pkgbuild/cmd/edge/input.proto

package main

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
	unsafe "unsafe"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Proto message which reflects the input properties fora the pkgbuild recipe.
// See more detailed description for each field there.
type Input struct {
	state protoimpl.MessageState `protogen:"open.v1"`
	// Target CIPD platform for packages.
	TargetPlatform string `protobuf:"bytes,2,opt,name=target_platform,json=targetPlatform,proto3" json:"target_platform,omitempty"`
	// Storage directory for build and cache packages.
	StorageDir string `protobuf:"bytes,3,opt,name=storage_dir,json=storageDir,proto3" json:"storage_dir,omitempty"`
	// Spec pool directory for finding 3pp specs.
	SpecPool string `protobuf:"bytes,4,opt,name=spec_pool,json=specPool,proto3" json:"spec_pool,omitempty"`
	// If true, packages will check new versions. Otherwise source lock will be
	// used. If the package is without both update and source lock, build will
	// failed.
	Update bool `protobuf:"varint,10,opt,name=update,proto3" json:"update,omitempty"`
	// If true, packages will be built.
	Build bool `protobuf:"varint,11,opt,name=build,proto3" json:"build,omitempty"`
	// If true, upload packages to CIPD service.
	Upload bool `protobuf:"varint,6,opt,name=upload,proto3" json:"upload,omitempty"`
	// If true, source lock file under the package spec directory will be updated.
	UpdateSourceLock bool `protobuf:"varint,9,opt,name=update_source_lock,json=updateSourceLock,proto3" json:"update_source_lock,omitempty"`
	// List of source platforms to be updated.
	UpdateSourcePlatforms []string `protobuf:"bytes,12,rep,name=update_source_platforms,json=updateSourcePlatforms,proto3" json:"update_source_platforms,omitempty"`
	// CIPD service URL for downloading and uploading packages.
	CipdService string `protobuf:"bytes,5,opt,name=cipd_service,json=cipdService,proto3" json:"cipd_service,omitempty"`
	// The prefix to use for uploading built packages.
	CipdPackagePrefix string `protobuf:"bytes,7,opt,name=cipd_package_prefix,json=cipdPackagePrefix,proto3" json:"cipd_package_prefix,omitempty"`
	// Snoopy service URL for reporting artifact hash.
	SnoopyService string `protobuf:"bytes,8,opt,name=snoopy_service,json=snoopyService,proto3" json:"snoopy_service,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *Input) Reset() {
	*x = Input{}
	mi := &file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *Input) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Input) ProtoMessage() {}

func (x *Input) ProtoReflect() protoreflect.Message {
	mi := &file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Input.ProtoReflect.Descriptor instead.
func (*Input) Descriptor() ([]byte, []int) {
	return file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescGZIP(), []int{0}
}

func (x *Input) GetTargetPlatform() string {
	if x != nil {
		return x.TargetPlatform
	}
	return ""
}

func (x *Input) GetStorageDir() string {
	if x != nil {
		return x.StorageDir
	}
	return ""
}

func (x *Input) GetSpecPool() string {
	if x != nil {
		return x.SpecPool
	}
	return ""
}

func (x *Input) GetUpdate() bool {
	if x != nil {
		return x.Update
	}
	return false
}

func (x *Input) GetBuild() bool {
	if x != nil {
		return x.Build
	}
	return false
}

func (x *Input) GetUpload() bool {
	if x != nil {
		return x.Upload
	}
	return false
}

func (x *Input) GetUpdateSourceLock() bool {
	if x != nil {
		return x.UpdateSourceLock
	}
	return false
}

func (x *Input) GetUpdateSourcePlatforms() []string {
	if x != nil {
		return x.UpdateSourcePlatforms
	}
	return nil
}

func (x *Input) GetCipdService() string {
	if x != nil {
		return x.CipdService
	}
	return ""
}

func (x *Input) GetCipdPackagePrefix() string {
	if x != nil {
		return x.CipdPackagePrefix
	}
	return ""
}

func (x *Input) GetSnoopyService() string {
	if x != nil {
		return x.SnoopyService
	}
	return ""
}

var File_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto protoreflect.FileDescriptor

var file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDesc = string([]byte{
	0x0a, 0x39, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72,
	0x67, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x74, 0x6f, 0x6f, 0x6c, 0x73, 0x2f, 0x70, 0x6b,
	0x67, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x2f, 0x63, 0x6d, 0x64, 0x2f, 0x65, 0x64, 0x67, 0x65, 0x2f,
	0x69, 0x6e, 0x70, 0x75, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x6d, 0x61, 0x69,
	0x6e, 0x22, 0x94, 0x03, 0x0a, 0x05, 0x49, 0x6e, 0x70, 0x75, 0x74, 0x12, 0x27, 0x0a, 0x0f, 0x74,
	0x61, 0x72, 0x67, 0x65, 0x74, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x74, 0x61, 0x72, 0x67, 0x65, 0x74, 0x50, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x12, 0x1f, 0x0a, 0x0b, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x5f,
	0x64, 0x69, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x74, 0x6f, 0x72, 0x61,
	0x67, 0x65, 0x44, 0x69, 0x72, 0x12, 0x1b, 0x0a, 0x09, 0x73, 0x70, 0x65, 0x63, 0x5f, 0x70, 0x6f,
	0x6f, 0x6c, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x73, 0x70, 0x65, 0x63, 0x50, 0x6f,
	0x6f, 0x6c, 0x12, 0x16, 0x0a, 0x06, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x18, 0x0a, 0x20, 0x01,
	0x28, 0x08, 0x52, 0x06, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x62, 0x75,
	0x69, 0x6c, 0x64, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05, 0x62, 0x75, 0x69, 0x6c, 0x64,
	0x12, 0x16, 0x0a, 0x06, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x08,
	0x52, 0x06, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x2c, 0x0a, 0x12, 0x75, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x5f, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x5f, 0x6c, 0x6f, 0x63, 0x6b, 0x18, 0x09,
	0x20, 0x01, 0x28, 0x08, 0x52, 0x10, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x53, 0x6f, 0x75, 0x72,
	0x63, 0x65, 0x4c, 0x6f, 0x63, 0x6b, 0x12, 0x36, 0x0a, 0x17, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x5f, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x5f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x73, 0x18, 0x0c, 0x20, 0x03, 0x28, 0x09, 0x52, 0x15, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x53,
	0x6f, 0x75, 0x72, 0x63, 0x65, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x73, 0x12, 0x21,
	0x0a, 0x0c, 0x63, 0x69, 0x70, 0x64, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63, 0x69, 0x70, 0x64, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x2e, 0x0a, 0x13, 0x63, 0x69, 0x70, 0x64, 0x5f, 0x70, 0x61, 0x63, 0x6b, 0x61, 0x67,
	0x65, 0x5f, 0x70, 0x72, 0x65, 0x66, 0x69, 0x78, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x11,
	0x63, 0x69, 0x70, 0x64, 0x50, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x50, 0x72, 0x65, 0x66, 0x69,
	0x78, 0x12, 0x25, 0x0a, 0x0e, 0x73, 0x6e, 0x6f, 0x6f, 0x70, 0x79, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x6e, 0x6f, 0x6f, 0x70,
	0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x42, 0x34, 0x5a, 0x32, 0x67, 0x6f, 0x2e, 0x63,
	0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x69, 0x6e, 0x66, 0x72,
	0x61, 0x2f, 0x74, 0x6f, 0x6f, 0x6c, 0x73, 0x2f, 0x70, 0x6b, 0x67, 0x62, 0x75, 0x69, 0x6c, 0x64,
	0x2f, 0x63, 0x6d, 0x64, 0x2f, 0x65, 0x64, 0x67, 0x65, 0x3b, 0x6d, 0x61, 0x69, 0x6e, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
})

var (
	file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescOnce sync.Once
	file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescData []byte
)

func file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescGZIP() []byte {
	file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescOnce.Do(func() {
		file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescData = protoimpl.X.CompressGZIP(unsafe.Slice(unsafe.StringData(file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDesc), len(file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDesc)))
	})
	return file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDescData
}

var file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_goTypes = []any{
	(*Input)(nil), // 0: main.Input
}
var file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_init() }
func file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_init() {
	if File_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: unsafe.Slice(unsafe.StringData(file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDesc), len(file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_rawDesc)),
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_goTypes,
		DependencyIndexes: file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_depIdxs,
		MessageInfos:      file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_msgTypes,
	}.Build()
	File_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto = out.File
	file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_goTypes = nil
	file_go_chromium_org_infra_tools_pkgbuild_cmd_edge_input_proto_depIdxs = nil
}
