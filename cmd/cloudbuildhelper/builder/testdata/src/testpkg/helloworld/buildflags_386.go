// Copyright 2020 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import "testpkg/nope"

// B1 exists to make golint happy.
const B1 = nope.A
