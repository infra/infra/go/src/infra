// Copyright 2025 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package caching provides interfaces for managing DataStore entities that correspond to the caching service
package caching
