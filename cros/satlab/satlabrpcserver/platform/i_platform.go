// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
package platform

// IPlatform provides the different functions in different platforms.
// e.g. reboot
type IPlatform interface{}
