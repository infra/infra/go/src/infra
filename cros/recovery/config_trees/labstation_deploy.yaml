plans:
    - name: cros
      criticalactions:
        - name: 'Set state: needs_deploy'
          exec_name: dut_set_state
          exec_args:
            - state:needs_deploy
          docs:
            - The action set devices with request to be redeployed.
        - name: check_host_info (RUN_ONCE)
          exec_name: sample_pass
          docs:
            - Check basic info for deployment.
          dependencies:
            - name: dut_has_board_name
              exec_name: dut_has_board_name
            - name: dut_has_model_name
              exec_name: dut_has_model_name
        - name: Collect logs (before) (Allow to fail) (RUN_ONCE)
          exec_name: sample_pass
          docs:
            - Collect any pre-existing logs from before deletes such logs.
            - 'Any logs collection are not critical, and we marks '
            - that action attempt to perform to avoid repeating it.
            - 'Collection to folder: before.'
          conditions:
            - name: Device is SSHable (ALWAYS_RUN) (time:'30s')
              exec_name: cros_ssh
              docs:
                - This verifier checks whether the host is accessible over ssh.
            - name: Confirm log collection info does not exist (before)
              exec_name: cros_confirm_file_not_exists
              exec_args:
                - target_file:log_collection_info_before
              docs:
                - 'Need to check whether the log collection info file already '
                - 'exists in the file system. A pre-existing file indicates that '
                - 'the collection of any pre-existing logs has already been '
                - tried to be collected.
          dependencies:
            - name: Create log collection info (before) (RUN_ONCE)
              exec_name: cros_create_log_collection_info
              exec_args:
                - info_file:log_collection_info_before
              docs:
                - 'When the log collection completes, we create an info file that '
                - indicates the successful completion of the collection process.
              conditions:
                - name: Confirm log collection info does not exist (before)
                  exec_name: cros_confirm_file_not_exists
                  exec_args:
                    - target_file:log_collection_info_before
                  docs:
                    - 'Need to check whether the log collection info file already '
                    - 'exists in the file system. A pre-existing file indicates that '
                    - 'the collection of any pre-existing logs has already been '
                    - tried to be collected.
            - name: Copy messages (before) (Allow to fail)
              exec_name: cros_copy_to_logs
              exec_args:
                - src_host_type:dut
                - src_path:/var/log/messages
                - src_type:file
                - custom_dir:before
              docs:
                - Try to collect /var/log/messages.
                - 'Collection to folder: before.'
            - name: Copy eventlog.txt (before) (Allow to fail)
              exec_name: cros_copy_to_logs
              exec_args:
                - src_host_type:dut
                - src_path:/var/log/eventlog.txt
                - src_type:file
                - custom_dir:before
              docs:
                - Try to collect /var/log/eventlog.txt.
                - 'Collection to folder: before.'
            - name: Collect dmesg (before) (Allow to fail) (RUN_ONCE)
              exec_name: cros_dmesg
              exec_args:
                - human_readable:false
                - device_type:dut
                - custom_dir:before
              docs:
                - Collect the dmesg output.
                - 'Collection to folder: before.'
        - name: Device is SSHable (ALWAYS_RUN) (time:'30s')
          exec_name: cros_ssh
          docs:
            - This verifier checks whether the host is accessible over ssh.
          recoveries:
            - name: Power cycle by RPM (ALWAYS_RUN) (time:'2m0s')
              exec_name: rpm_power_cycle
              docs:
                - Power cycle the labstation via RPM.
              conditions:
                - name: rpm_action_enabled
                  exec_name: rpm_action_enabled
                - name: has_rpm_info
                  exec_name: has_rpm_info
        - name: Update inventory info
          exec_name: sample_pass
          docs:
            - Updating device info in inventory.
          dependencies:
            - name: cros_update_hwid_to_inventory
              exec_name: cros_update_hwid_to_inventory
            - name: cros_update_serial_number_inventory
              exec_name: cros_update_serial_number_inventory
        - name: Remove whitelabel_tag VPD field if necessary
          exec_name: cros_run_command
          exec_args:
            - 'host:'
            - command:vpd -d whitelabel_tag
          docs:
            - Remove whitelabel_tag field from VPD cache when the field exists.
          conditions:
            - name: VPD cache has whitelabel_tag field
              exec_name: cros_run_command
              exec_args:
                - 'host:'
                - command:vpd -l | grep whitelabel_tag
              docs:
                - Check if unwanted whitelabel_tag field in VPD cache.
        - name: Installed OS is stable
          exec_name: cros_is_on_stable_version
          docs:
            - Verify that OS on the device is stable.
            - Labstation will be rebooted to make it ready for use.
          conditions:
            - name: has_stable_version_cros_image
              exec_name: has_stable_version_cros_image
          recoveries:
            - name: Install stable OS (time:'1h0m0s')
              exec_name: cros_provision
              docs:
                - Install stable OS on the device.
                - Labstation will be rebooted to make it ready for use.
              conditions:
                - name: has_stable_version_cros_image
                  exec_name: has_stable_version_cros_image
                - name: cros_not_on_stable_version
                  exec_name: cros_not_on_stable_version
            - name: Power cycle by RPM (ALWAYS_RUN) (time:'2m0s')
              exec_name: rpm_power_cycle
              docs:
                - Power cycle the labstation via RPM.
              conditions:
                - name: rpm_action_enabled
                  exec_name: rpm_action_enabled
                - name: has_rpm_info
                  exec_name: has_rpm_info
        - name: Remove reboot requests from host (Allow to fail)
          exec_name: cros_remove_all_reboot_request
          docs:
            - Remove reboot request flag files.
        - name: Update provisioned info
          exec_name: cros_update_provision_info
          docs:
            - Update OS version for provision info.
        - name: Validate RPM info (time:'15m0s')
          exec_name: rpm_audit_without_battery
          docs:
            - Validate and update rpm_state.
            - The execs is not ready yet.
          conditions:
            - name: rpm_action_enabled
              exec_name: rpm_action_enabled
            - name: has_rpm_info
              exec_name: has_rpm_info
          recoveries:
            - name: Power cycle by RPM (ALWAYS_RUN) (time:'2m0s')
              exec_name: rpm_power_cycle
              docs:
                - Power cycle the labstation via RPM.
              conditions:
                - name: rpm_action_enabled
                  exec_name: rpm_action_enabled
                - name: has_rpm_info
                  exec_name: has_rpm_info
        - name: 'Set state: ready (RUN_ONCE)'
          exec_name: dut_set_state
          exec_args:
            - state:ready
          docs:
            - The action set devices with state ready for the testing.
          dependencies:
            - name: All repair-requests resolved
              exec_name: dut_has_no_repair_requests
              docs:
                - Checks if all repair requests are resolved
            - name: Reset DUT-state reason
              exec_name: dut_reset_state_reason
              docs:
                - Reset DUT-state-reason for good DUT as it becomes stale.
        - name: Collect logs (after) (Allow to fail) (RUN_ONCE)
          exec_name: sample_pass
          docs:
            - Collect any pre-existing logs from before deletes such logs.
            - 'Any logs collection are not critical, and we marks '
            - that action attempt to perform to avoid repeating it.
            - 'Collection to folder: after.'
          conditions:
            - name: Device is SSHable (ALWAYS_RUN) (time:'30s')
              exec_name: cros_ssh
              docs:
                - This verifier checks whether the host is accessible over ssh.
            - name: Confirm log collection info does not exist (after)
              exec_name: cros_confirm_file_not_exists
              exec_args:
                - target_file:log_collection_info_after
              docs:
                - 'Need to check whether the log collection info file already '
                - 'exists in the file system. A pre-existing file indicates that '
                - 'the collection of any pre-existing logs has already been '
                - tried to be collected.
          dependencies:
            - name: Create log collection info (after) (RUN_ONCE)
              exec_name: cros_create_log_collection_info
              exec_args:
                - info_file:log_collection_info_after
              docs:
                - 'When the log collection completes, we create an info file that '
                - indicates the successful completion of the collection process.
              conditions:
                - name: Confirm log collection info does not exist (after)
                  exec_name: cros_confirm_file_not_exists
                  exec_args:
                    - target_file:log_collection_info_after
                  docs:
                    - 'Need to check whether the log collection info file already '
                    - 'exists in the file system. A pre-existing file indicates that '
                    - 'the collection of any pre-existing logs has already been '
                    - tried to be collected.
            - name: Copy messages (after) (Allow to fail)
              exec_name: cros_copy_to_logs
              exec_args:
                - src_host_type:dut
                - src_path:/var/log/messages
                - src_type:file
                - custom_dir:after
              docs:
                - Try to collect /var/log/messages.
                - 'Collection to folder: after.'
            - name: Copy eventlog.txt (after) (Allow to fail)
              exec_name: cros_copy_to_logs
              exec_args:
                - src_host_type:dut
                - src_path:/var/log/eventlog.txt
                - src_type:file
                - custom_dir:after
              docs:
                - Try to collect /var/log/eventlog.txt.
                - 'Collection to folder: after.'
            - name: Collect dmesg (after) (Allow to fail) (RUN_ONCE)
              exec_name: cros_dmesg
              exec_args:
                - human_readable:false
                - device_type:dut
                - custom_dir:after
              docs:
                - Collect the dmesg output.
                - 'Collection to folder: after.'
