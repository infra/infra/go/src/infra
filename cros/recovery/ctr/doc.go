// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package ctr represents function and interfaces to communitae with CTR CIPD.
package ctr
