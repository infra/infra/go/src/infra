// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//

// Code generated by MockGen. DO NOT EDIT.
// Source: metrics.go

// Package mockmetrics is a generated GoMock package.
package mockmetrics

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	metrics "go.chromium.org/infra/cros/recovery/logger/metrics"
)

// MockMetrics is a mock of Metrics interface.
type MockMetrics struct {
	ctrl     *gomock.Controller
	recorder *MockMetricsMockRecorder
}

// MockMetricsMockRecorder is the mock recorder for MockMetrics.
type MockMetricsMockRecorder struct {
	mock *MockMetrics
}

// NewMockMetrics creates a new mock instance.
func NewMockMetrics(ctrl *gomock.Controller) *MockMetrics {
	mock := &MockMetrics{ctrl: ctrl}
	mock.recorder = &MockMetricsMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockMetrics) EXPECT() *MockMetricsMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockMetrics) Create(ctx context.Context, action *metrics.Action) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", ctx, action)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockMetricsMockRecorder) Create(ctx, action interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockMetrics)(nil).Create), ctx, action)
}

// Search mocks base method.
func (m *MockMetrics) Search(ctx context.Context, q *metrics.Query) (*metrics.QueryResult, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Search", ctx, q)
	ret0, _ := ret[0].(*metrics.QueryResult)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Search indicates an expected call of Search.
func (mr *MockMetricsMockRecorder) Search(ctx, q interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Search", reflect.TypeOf((*MockMetrics)(nil).Search), ctx, q)
}
