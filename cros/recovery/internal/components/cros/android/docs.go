// Copyright 2025 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package android contains functions to work with AndroidOS on ChromeOS devices.
package android
