# proto-file: third_party/chromiumos_config/proto/chromiumos/test/api/ctp2.proto
# proto-message: InternalTestplan
test_cases {
  name: "tauto.hardware_TPMCheck"
  metadata {
    test_case {
      id {
        value: "tauto.hardware_TPMCheck"
      }
      name: "hardware_TPMCheck"
      tags {
        value: "suite:pvs-kernel"
      }
      tags {
        value: "suite:faft_lv1"
      }
      tags {
        value: "suite:kernel_per-build_regression"
      }
      tags {
        value: "suite:faft_bios_rw_qual"
      }
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      tags {
        value: "test_class:hardware"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "seancarpenter@google.com"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Used during AP/EC firmware suspend stress testing"
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.power_CPUIdle"
  metadata {
    test_case {
      id {
        value: "tauto.power_CPUIdle"
      }
      name: "power_CPUIdle"
      tags {
        value: "suite:faft_lv1"
      }
      tags {
        value: "release-health_power"
      }
      tags {
        value: "suite:faft_bios_rw_qual"
      }
      tags {
        value: "group:release-health"
      }
      tags {
        value: "suite:power_build"
      }
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "cros-power-notifications@google.com"
      }
      bug_component {
        value: "b:1361410"
      }
      criteria {
        value: "Fails if the cpu did not have any idle cycles during this test."
      }
      hw_agnostic {
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.firmware_CbfsMcache"
  metadata {
    test_case {
      id {
        value: "tauto.firmware_CbfsMcache"
      }
      name: "firmware_CbfsMcache"
      tags {
        value: "suite:faft_bios_rw_qual"
      }
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      tags {
        value: "suite:faft_lv4"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Ensure the CBFS metadata cache did not overflow"
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.power_CPUFreq"
  metadata {
    test_case {
      id {
        value: "tauto.power_CPUFreq"
      }
      name: "power_CPUFreq"
      tags {
        value: "suite:faft_lv1"
      }
      tags {
        value: "release-health_power"
      }
      tags {
        value: "suite:faft_bios_rw_qual"
      }
      tags {
        value: "group:release-health"
      }
      tags {
        value: "suite:power_build"
      }
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "cros-power-notifications@google.com"
      }
      bug_component {
        value: "b:1361410"
      }
      criteria {
        value: "This test will fail under the following conditions:\n                    - cpu frequencies are not supported\n                    - no more than 1 frequency is supported\n                    - if a supported frequency cannot be set"
      }
      hw_agnostic {
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.firmware_CorruptMinios.minios_a"
  metadata {
    test_case {
      id {
        value: "tauto.firmware_CorruptMinios.minios_a"
      }
      name: "firmware_CorruptMinios.minios_a"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      tags {
        value: "suite:faft_lv4"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Corrupt one MiniOS kernel, set the priority to that, and ensure that both priority can boot"
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.firmware_CorruptMinios.minios_b"
  metadata {
    test_case {
      id {
        value: "tauto.firmware_CorruptMinios.minios_b"
      }
      name: "firmware_CorruptMinios.minios_b"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      tags {
        value: "suite:faft_lv4"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Corrupt one MiniOS kernel, set the priority to that, and ensure that both priority can boot"
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.firmware_WilcoDiagnosticsMode"
  metadata {
    test_case {
      id {
        value: "tauto.firmware_WilcoDiagnosticsMode"
      }
      name: "firmware_WilcoDiagnosticsMode"
      tags {
        value: "suite:faft_bios_rw_qual"
      }
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      tags {
        value: "suite:faft_wilco"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
      dependencies {
        value: "label-board:sarien"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Verify that AP firmware handles a corrupt Wilco diagnostics binary and applies updates to it"
      }
      life_cycle_stage {
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
          swarming_labels: "label-board:sarien"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios_ccd"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios_ccd"
      }
      name: "tast.firmware-bios_ccd"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_component:ccd_gsc"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for AP firmware that require CCD"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_component:ccd_gsc"
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios_usb-0"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios_usb-0"
      }
      name: "tast.firmware-bios_usb-0"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
      dependencies {
        value: "label-servo_usb_state:NORMAL"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for firmware that require USB"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
          swarming_labels: "label-servo_usb_state:NORMAL"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios_usb-1"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios_usb-1"
      }
      name: "tast.firmware-bios_usb-1"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
      dependencies {
        value: "label-servo_usb_state:NORMAL"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for firmware that require USB"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
          swarming_labels: "label-servo_usb_state:NORMAL"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios-shard-0"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios-shard-0"
      }
      name: "tast.firmware-bios-shard-0"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for AP firmware"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios-shard-3"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios-shard-3"
      }
      name: "tast.firmware-bios-shard-3"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for AP firmware"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios-shard-2"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios-shard-2"
      }
      name: "tast.firmware-bios-shard-2"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for AP firmware"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
test_cases {
  name: "tauto.tast.firmware-bios-shard-1"
  metadata {
    test_case {
      id {
        value: "tauto.tast.firmware-bios-shard-1"
      }
      name: "tast.firmware-bios-shard-1"
      tags {
        value: "suite:faft_bios"
      }
      tags {
        value: "suite:faft_bios_ro_qual"
      }
      dependencies {
        value: "label-servo_state:WORKING"
      }
    }
    test_case_exec {
      test_harness {
        tauto {
        }
      }
    }
    test_case_info {
      owners {
        email: "chromeos-faft@google.com"
      }
      owners {
        email: "jbettis@chromium.org"
      }
      bug_component {
        value: "b:792402"
      }
      criteria {
        value: "Run Tast tests for AP firmware"
      }
      life_cycle_stage {
        value: LIFE_CYCLE_OWNER_MONITORED
      }
    }
  }
  scheduling_unit_options {
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          swarming_labels: "label-servo_state:WORKING"
        }
        sw_req {
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
  }
}
suite_info {
  suite_metadata {
    pool: "faft-test"
    execution_metadata {
      args {
        flag: "is_partner_run"
        value: "false"
      }
      args {
        flag: "is_al_run"
        value: "false"
      }
    }
    scheduler_info {
      scheduler: SCHEDUKE
    }
    dynamic_updates {
      focal_task_finder {
        first {
          task_type: TEST
        }
      }
      update_action {
        insert {
          insert_type: PREPEND
          task {
            ordered_container_requests {
              dynamic_identifier: "servo-nexus_primary"
              container {
                generic {
                  binary_name: "cros-servod"
                  binary_args: "server"
                  binary_args: "-server_port"
                  binary_args: "0"
                  docker_artifact_dir: "/tmp/servod"
                  additional_volumes: "/creds:/creds"
                }
              }
              network: "adbnet"
              container_image_key: "servo-nexus"
            }
            ordered_container_requests {
              dynamic_identifier: "crosDutServer_primary"
              container {
                cros_dut {
                }
              }
              dynamic_deps {
                key: "crosDut.dutAddress"
                value: "device_primary.dutServer"
              }
              dynamic_deps {
                key: "crosDut.cacheServer"
                value: "cache-server"
              }
              container_image_key: "cros-dut"
            }
            ordered_container_requests {
              dynamic_identifier: "cros-provision_primary"
              container {
                generic {
                  binary_name: "cros-provision"
                  binary_args: "server"
                  binary_args: "-port"
                  binary_args: "0"
                  docker_artifact_dir: "/tmp/provisionservice"
                  additional_volumes: "/creds:/creds"
                }
              }
              container_image_key: "cros-provision"
            }
            provision {
              service_address {
              }
              startup_request {
              }
              install_request {
                image_path {
                  host_type: GS
                  path: "${installPath}"
                }
                metadata {
                  type_url: "type.googleapis.com/chromiumos.test.api.CrOSProvisionMetadata"
                }
              }
              dynamic_deps {
                key: "serviceAddress"
                value: "cros-provision_primary"
              }
              dynamic_deps {
                key: "startupRequest.dut"
                value: "device_primary.dut"
              }
              dynamic_deps {
                key: "startupRequest.dutServer"
                value: "crosDutServer_primary"
              }
              dynamic_deps {
                key: "startupRequest.servoNexusAddr"
                value: "servo-nexus_primary"
              }
              target: "primary"
              dynamic_identifier: "cros-provision_primary"
            }
          }
        }
      }
    }
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "rex"
                model_name: "karis"
              }
            }
          }
          provision_info {
            install_request {
              image_path {
                host_type: GS
                path: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
              }
            }
          }
        }
        sw_req {
          build: "release"
          gcs_path: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
          key_values {
            key: "chromeos_build"
            value: "rex-release/R135-16202.0.0"
          }
          key_values {
            key: "chromeos_build_gcs_bucket"
            value: "chromeos-image-archive"
          }
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "rex"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/rex-release/R135-16202.0.0"
      }
    }
    scheduling_units {
      primary_target {
        swarming_def {
          dut_info {
            chromeos {
              dut_model {
                build_target: "reven-vmtest"
              }
            }
          }
          provision_info {
            install_request {
              image_path {
                host_type: GS
                path: "gs://chromeos-image-archive/reven-vmtest-release/R135-16202.0.0"
              }
            }
          }
        }
      }
      dynamic_update_lookup_table {
        key: "board"
        value: "reven-vmtest"
      }
      dynamic_update_lookup_table {
        key: "installPath"
        value: "gs://chromeos-image-archive/reven-vmtest-release/R135-16202.0.0"
      }
    }
  }
  suite_request {
    test_suite {
      name: "faft_bios"
      test_case_tag_criteria {
        tags: "suite:faft_bios"
      }
    }
    maximum_duration {
      seconds: 140400
    }
    analytics_name: "FaftBiosWeekly"
    max_in_shard: 1
  }
}
