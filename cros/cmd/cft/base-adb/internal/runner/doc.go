// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package runner provides tool to run blocking ADB commands and provide output.
package runner
