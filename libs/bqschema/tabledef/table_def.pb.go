// Copyright 2017 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.5
// 	protoc        v5.29.3
// source: infra/libs/bqschema/tabledef/table_def.proto

package tabledef

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
	unsafe "unsafe"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Type int32

const (
	Type_STRING    Type = 0
	Type_BYTES     Type = 1
	Type_INTEGER   Type = 2
	Type_FLOAT     Type = 3
	Type_BOOLEAN   Type = 4
	Type_TIMESTAMP Type = 5
	Type_RECORD    Type = 6
	Type_DATE      Type = 7
	Type_TIME      Type = 8
	Type_DATETIME  Type = 9
)

// Enum value maps for Type.
var (
	Type_name = map[int32]string{
		0: "STRING",
		1: "BYTES",
		2: "INTEGER",
		3: "FLOAT",
		4: "BOOLEAN",
		5: "TIMESTAMP",
		6: "RECORD",
		7: "DATE",
		8: "TIME",
		9: "DATETIME",
	}
	Type_value = map[string]int32{
		"STRING":    0,
		"BYTES":     1,
		"INTEGER":   2,
		"FLOAT":     3,
		"BOOLEAN":   4,
		"TIMESTAMP": 5,
		"RECORD":    6,
		"DATE":      7,
		"TIME":      8,
		"DATETIME":  9,
	}
)

func (x Type) Enum() *Type {
	p := new(Type)
	*p = x
	return p
}

func (x Type) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Type) Descriptor() protoreflect.EnumDescriptor {
	return file_infra_libs_bqschema_tabledef_table_def_proto_enumTypes[0].Descriptor()
}

func (Type) Type() protoreflect.EnumType {
	return &file_infra_libs_bqschema_tabledef_table_def_proto_enumTypes[0]
}

func (x Type) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Type.Descriptor instead.
func (Type) EnumDescriptor() ([]byte, []int) {
	return file_infra_libs_bqschema_tabledef_table_def_proto_rawDescGZIP(), []int{0}
}

type TableDef struct {
	state protoimpl.MessageState `protogen:"open.v1"`
	// Name of the dataset.
	DatasetId string `protobuf:"bytes,1,opt,name=dataset_id,json=datasetId,proto3" json:"dataset_id,omitempty"`
	// Name of the table.
	TableId string `protobuf:"bytes,2,opt,name=table_id,json=tableId,proto3" json:"table_id,omitempty"`
	// Human-readable name of the table. (optional)
	Name string `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	// Human-readable description of the table. (optional)
	Description string         `protobuf:"bytes,4,opt,name=description,proto3" json:"description,omitempty"`
	Fields      []*FieldSchema `protobuf:"bytes,5,rep,name=fields,proto3" json:"fields,omitempty"`
	// Whether to partition the table by day.
	PartitionTable bool `protobuf:"varint,6,opt,name=partition_table,json=partitionTable,proto3" json:"partition_table,omitempty"`
	// The lifetime for each partition. If zero, partitions do not expire.
	// Ignored if partition_table is false. (optional)
	PartitionExpirationSeconds int64 `protobuf:"varint,7,opt,name=partition_expiration_seconds,json=partitionExpirationSeconds,proto3" json:"partition_expiration_seconds,omitempty"`
	unknownFields              protoimpl.UnknownFields
	sizeCache                  protoimpl.SizeCache
}

func (x *TableDef) Reset() {
	*x = TableDef{}
	mi := &file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *TableDef) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TableDef) ProtoMessage() {}

func (x *TableDef) ProtoReflect() protoreflect.Message {
	mi := &file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TableDef.ProtoReflect.Descriptor instead.
func (*TableDef) Descriptor() ([]byte, []int) {
	return file_infra_libs_bqschema_tabledef_table_def_proto_rawDescGZIP(), []int{0}
}

func (x *TableDef) GetDatasetId() string {
	if x != nil {
		return x.DatasetId
	}
	return ""
}

func (x *TableDef) GetTableId() string {
	if x != nil {
		return x.TableId
	}
	return ""
}

func (x *TableDef) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *TableDef) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *TableDef) GetFields() []*FieldSchema {
	if x != nil {
		return x.Fields
	}
	return nil
}

func (x *TableDef) GetPartitionTable() bool {
	if x != nil {
		return x.PartitionTable
	}
	return false
}

func (x *TableDef) GetPartitionExpirationSeconds() int64 {
	if x != nil {
		return x.PartitionExpirationSeconds
	}
	return 0
}

type FieldSchema struct {
	state protoimpl.MessageState `protogen:"open.v1"`
	// Name of the field/column.
	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	// Human-readable description of the field. (optional)
	Description   string         `protobuf:"bytes,2,opt,name=description,proto3" json:"description,omitempty"`
	IsRepeated    bool           `protobuf:"varint,3,opt,name=is_repeated,json=isRepeated,proto3" json:"is_repeated,omitempty"`
	IsRequired    bool           `protobuf:"varint,4,opt,name=is_required,json=isRequired,proto3" json:"is_required,omitempty"`
	Type          Type           `protobuf:"varint,5,opt,name=type,proto3,enum=tabledef.Type" json:"type,omitempty"`
	Schema        []*FieldSchema `protobuf:"bytes,6,rep,name=schema,proto3" json:"schema,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *FieldSchema) Reset() {
	*x = FieldSchema{}
	mi := &file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes[1]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *FieldSchema) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FieldSchema) ProtoMessage() {}

func (x *FieldSchema) ProtoReflect() protoreflect.Message {
	mi := &file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes[1]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FieldSchema.ProtoReflect.Descriptor instead.
func (*FieldSchema) Descriptor() ([]byte, []int) {
	return file_infra_libs_bqschema_tabledef_table_def_proto_rawDescGZIP(), []int{1}
}

func (x *FieldSchema) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *FieldSchema) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *FieldSchema) GetIsRepeated() bool {
	if x != nil {
		return x.IsRepeated
	}
	return false
}

func (x *FieldSchema) GetIsRequired() bool {
	if x != nil {
		return x.IsRequired
	}
	return false
}

func (x *FieldSchema) GetType() Type {
	if x != nil {
		return x.Type
	}
	return Type_STRING
}

func (x *FieldSchema) GetSchema() []*FieldSchema {
	if x != nil {
		return x.Schema
	}
	return nil
}

var File_infra_libs_bqschema_tabledef_table_def_proto protoreflect.FileDescriptor

var file_infra_libs_bqschema_tabledef_table_def_proto_rawDesc = string([]byte{
	0x0a, 0x2c, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x6c, 0x69, 0x62, 0x73, 0x2f, 0x62, 0x71, 0x73,
	0x63, 0x68, 0x65, 0x6d, 0x61, 0x2f, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x64, 0x65, 0x66, 0x2f, 0x74,
	0x61, 0x62, 0x6c, 0x65, 0x5f, 0x64, 0x65, 0x66, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08,
	0x74, 0x61, 0x62, 0x6c, 0x65, 0x64, 0x65, 0x66, 0x22, 0x94, 0x02, 0x0a, 0x08, 0x54, 0x61, 0x62,
	0x6c, 0x65, 0x44, 0x65, 0x66, 0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x61, 0x74, 0x61, 0x73, 0x65, 0x74,
	0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x64, 0x61, 0x74, 0x61, 0x73,
	0x65, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x49, 0x64, 0x12,
	0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x2d, 0x0a, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x18,
	0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x64, 0x65, 0x66,
	0x2e, 0x46, 0x69, 0x65, 0x6c, 0x64, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x61, 0x52, 0x06, 0x66, 0x69,
	0x65, 0x6c, 0x64, 0x73, 0x12, 0x27, 0x0a, 0x0f, 0x70, 0x61, 0x72, 0x74, 0x69, 0x74, 0x69, 0x6f,
	0x6e, 0x5f, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0e, 0x70,
	0x61, 0x72, 0x74, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x61, 0x62, 0x6c, 0x65, 0x12, 0x40, 0x0a,
	0x1c, 0x70, 0x61, 0x72, 0x74, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x65, 0x78, 0x70, 0x69, 0x72,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x73, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x1a, 0x70, 0x61, 0x72, 0x74, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x45, 0x78,
	0x70, 0x69, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x73, 0x22,
	0xd8, 0x01, 0x0a, 0x0b, 0x46, 0x69, 0x65, 0x6c, 0x64, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x61, 0x12,
	0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1f, 0x0a, 0x0b, 0x69, 0x73, 0x5f, 0x72, 0x65, 0x70, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0a, 0x69, 0x73, 0x52, 0x65,
	0x70, 0x65, 0x61, 0x74, 0x65, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x69, 0x73, 0x5f, 0x72, 0x65, 0x71,
	0x75, 0x69, 0x72, 0x65, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0a, 0x69, 0x73, 0x52,
	0x65, 0x71, 0x75, 0x69, 0x72, 0x65, 0x64, 0x12, 0x22, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x0e, 0x2e, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x64, 0x65, 0x66,
	0x2e, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x2d, 0x0a, 0x06, 0x73,
	0x63, 0x68, 0x65, 0x6d, 0x61, 0x18, 0x06, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x74, 0x61,
	0x62, 0x6c, 0x65, 0x64, 0x65, 0x66, 0x2e, 0x46, 0x69, 0x65, 0x6c, 0x64, 0x53, 0x63, 0x68, 0x65,
	0x6d, 0x61, 0x52, 0x06, 0x73, 0x63, 0x68, 0x65, 0x6d, 0x61, 0x2a, 0x7f, 0x0a, 0x04, 0x54, 0x79,
	0x70, 0x65, 0x12, 0x0a, 0x0a, 0x06, 0x53, 0x54, 0x52, 0x49, 0x4e, 0x47, 0x10, 0x00, 0x12, 0x09,
	0x0a, 0x05, 0x42, 0x59, 0x54, 0x45, 0x53, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x49, 0x4e, 0x54,
	0x45, 0x47, 0x45, 0x52, 0x10, 0x02, 0x12, 0x09, 0x0a, 0x05, 0x46, 0x4c, 0x4f, 0x41, 0x54, 0x10,
	0x03, 0x12, 0x0b, 0x0a, 0x07, 0x42, 0x4f, 0x4f, 0x4c, 0x45, 0x41, 0x4e, 0x10, 0x04, 0x12, 0x0d,
	0x0a, 0x09, 0x54, 0x49, 0x4d, 0x45, 0x53, 0x54, 0x41, 0x4d, 0x50, 0x10, 0x05, 0x12, 0x0a, 0x0a,
	0x06, 0x52, 0x45, 0x43, 0x4f, 0x52, 0x44, 0x10, 0x06, 0x12, 0x08, 0x0a, 0x04, 0x44, 0x41, 0x54,
	0x45, 0x10, 0x07, 0x12, 0x08, 0x0a, 0x04, 0x54, 0x49, 0x4d, 0x45, 0x10, 0x08, 0x12, 0x0c, 0x0a,
	0x08, 0x44, 0x41, 0x54, 0x45, 0x54, 0x49, 0x4d, 0x45, 0x10, 0x09, 0x42, 0x27, 0x5a, 0x25, 0x69,
	0x6e, 0x66, 0x72, 0x61, 0x2f, 0x6c, 0x69, 0x62, 0x73, 0x2f, 0x62, 0x71, 0x73, 0x63, 0x68, 0x65,
	0x6d, 0x61, 0x2f, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x64, 0x65, 0x66, 0x3b, 0x74, 0x61, 0x62, 0x6c,
	0x65, 0x64, 0x65, 0x66, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
})

var (
	file_infra_libs_bqschema_tabledef_table_def_proto_rawDescOnce sync.Once
	file_infra_libs_bqschema_tabledef_table_def_proto_rawDescData []byte
)

func file_infra_libs_bqschema_tabledef_table_def_proto_rawDescGZIP() []byte {
	file_infra_libs_bqschema_tabledef_table_def_proto_rawDescOnce.Do(func() {
		file_infra_libs_bqschema_tabledef_table_def_proto_rawDescData = protoimpl.X.CompressGZIP(unsafe.Slice(unsafe.StringData(file_infra_libs_bqschema_tabledef_table_def_proto_rawDesc), len(file_infra_libs_bqschema_tabledef_table_def_proto_rawDesc)))
	})
	return file_infra_libs_bqschema_tabledef_table_def_proto_rawDescData
}

var file_infra_libs_bqschema_tabledef_table_def_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_infra_libs_bqschema_tabledef_table_def_proto_goTypes = []any{
	(Type)(0),           // 0: tabledef.Type
	(*TableDef)(nil),    // 1: tabledef.TableDef
	(*FieldSchema)(nil), // 2: tabledef.FieldSchema
}
var file_infra_libs_bqschema_tabledef_table_def_proto_depIdxs = []int32{
	2, // 0: tabledef.TableDef.fields:type_name -> tabledef.FieldSchema
	0, // 1: tabledef.FieldSchema.type:type_name -> tabledef.Type
	2, // 2: tabledef.FieldSchema.schema:type_name -> tabledef.FieldSchema
	3, // [3:3] is the sub-list for method output_type
	3, // [3:3] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_infra_libs_bqschema_tabledef_table_def_proto_init() }
func file_infra_libs_bqschema_tabledef_table_def_proto_init() {
	if File_infra_libs_bqschema_tabledef_table_def_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: unsafe.Slice(unsafe.StringData(file_infra_libs_bqschema_tabledef_table_def_proto_rawDesc), len(file_infra_libs_bqschema_tabledef_table_def_proto_rawDesc)),
			NumEnums:      1,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_infra_libs_bqschema_tabledef_table_def_proto_goTypes,
		DependencyIndexes: file_infra_libs_bqschema_tabledef_table_def_proto_depIdxs,
		EnumInfos:         file_infra_libs_bqschema_tabledef_table_def_proto_enumTypes,
		MessageInfos:      file_infra_libs_bqschema_tabledef_table_def_proto_msgTypes,
	}.Build()
	File_infra_libs_bqschema_tabledef_table_def_proto = out.File
	file_infra_libs_bqschema_tabledef_table_def_proto_goTypes = nil
	file_infra_libs_bqschema_tabledef_table_def_proto_depIdxs = nil
}
