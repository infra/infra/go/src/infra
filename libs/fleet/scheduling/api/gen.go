// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//go:generate cproto

// Package api defines the API for managing test and task scheduling on Devices.
package api
