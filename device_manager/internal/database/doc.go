// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package database manages database connectors and implementations for the
// Device Lease service.
package database
