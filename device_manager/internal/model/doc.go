// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package model contains all the data models related to the Device Lease
// service. Each model implements CRUD operations for the entities as necessary.
package model
